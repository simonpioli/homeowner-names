<?php

namespace App\Services;

use App\DTO\Person;
use Illuminate\Contracts\Filesystem\FileNotFoundException;
use Illuminate\Support\Facades\Storage;
use Iterator;
use League\Csv\Exception;
use League\Csv\Reader;

class CsvProcessor
{
    const NAME_FIELD = 'homeowner';

    public function process(string $fileName): array
    {
        $rows = $this->load($fileName);
        $processedPeople = [];
        foreach ($rows as $key => $row) {
            // Split parts where 2 people in a row to array and process each one
            $people = preg_split('/( and )|( & )/', $row[self::NAME_FIELD]);
            $people = array_reverse($people);

            foreach ($people as $personNumber => $personString) {
                $processedPeople[] = (array) new Person($personString);
            }
        }

        // Second pass to tidy up married couple issues
        // Need to extract the first name from the next record as we can assume that the first person
        // has the initial or first name, not the 2nd as we just produced
        $processedPeople = $this->processMarriedPairs($processedPeople);

        return $processedPeople;
    }

    /**
     * @throws FileNotFoundException
     * @throws Exception
     */
    private function load(string $fileName): Iterator
    {
        $csv = Reader::createFromString(Storage::disk('local')->get($fileName));

        $csv->setHeaderOffset(0);

        // The supplied data may have empty columns, filtering them out
        $allHeaders = collect($csv->getHeader());
        $requiredHeaders = $allHeaders->filter()->all();

        return $csv->getRecords($requiredHeaders);
    }

    /**
     * Assumes the preceding person has the rest of the name.
     * Assumes the original ordering was male -> female and that the first name that's ended up
     * in the female entry is actually the name of the male
     */
    private function processMarriedPairs(array $people): array
    {
        foreach ($people as $key => $person) {
            if ($person['complete'] === false) {
                $people[$key]['first_name'] = $people[$key - 1]['first_name'];
                $people[$key]['initial'] = $people[$key - 1]['initial'];
                $people[$key]['last_name'] = $people[$key - 1]['last_name'];

                $people[$key - 1]['first_name'] = null;
                $people[$key - 1]['initial'] = null;

                $people[$key]['complete'] = true;
                $people[$key - 1]['error'] = true; // Setting this in case ordering is incorrect
            }
        }

        return $people;
    }
}
