<?php

namespace App\Http\Controllers\Api;

use App\Services\CsvProcessor;
use Illuminate\Contracts\Filesystem\FileNotFoundException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use League\Csv\Exception;
use RedisManager;

class HomeownerNamesController extends BaseController
{
    public function index(): JsonResponse
    {
        $homeowners = RedisManager::get('homeowners');

        return response()->json([
            'homeowners' => $homeowners,
        ]);
    }

    public function store(Request $request, CsvProcessor $csvProcessor): JsonResponse
    {
        $request->validate(['csv' => 'required|file']);

        try {
            $path = $request->csv->store(config('app.csv_location'));
            $processedData = $csvProcessor->process($path);
            RedisManager::set('homeowners', json_encode($processedData));
            return response()->json(['complete' => true], 201);
        } catch (FileNotFoundException $e) {
            report($e);
            abort(500, 'There was an error uploading your file, please try again');
        } catch (Exception $e) {
            report($e);
            abort(500, 'The file you uploaded appears to be invalid. Please check and try again');
        }
    }
}
