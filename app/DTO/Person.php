<?php

namespace App\DTO;

class Person
{
    /** @var string */
    public $title;

    /** @var string */
    public $first_name;

    /** @var string */
    public $initial;

    /** @var string */
    public $last_name;

    /** @var bool */
    public $error = false;

    /** @var bool */
    public $complete = true;

    /**
     * Person constructor.
     *
     * @param string|array $input
     */
    public function __construct($input)
    {
        if (is_string($input)) {
            $this->createFromRawString($input);

            if (!$this->title || !$this->last_name) {
                $this->error = true;
            }
        } else {
            // TODO: Implement create from array
        }

        return $this;
    }

    public function __toArray(): array
    {
        return [
            'title' => $this->title,
            'initial' => $this->initial,
            'first_name' => $this->first_name,
            'last_name' => $this->last_name,
            'complete' => $this->complete,
            'error' => $this->error,
        ];
    }

    public function createFromRawString(string $rawPerson): self
    {
        $processedPerson = $this->stripDots($rawPerson);

        // Split each name into bits
        $personRaw = explode(' ', $processedPerson);

        // First part's title
        $this->title = $this->sanitiseTitle($personRaw[0]);

        // Do we have a 'complete' name
        if (count($personRaw) > 1) {
            if (count($personRaw) > 2) {
                // Check if the 2nd part we have is a name or initial
                if (count(str_split($personRaw[1])) === 1) {
                    $this->initial = $personRaw[1];
                } else {
                    $this->first_name = $personRaw[1];
                }

                $this->last_name = $personRaw[2];

                // Pick up on extra words we've not considered before and set an error state.
                if (!empty($personRaw[3])) {
                    $this->error = true;
                }
            } else {
                $this->last_name = $personRaw[1];
            }
        } else {
            $this->complete = false;
        }

        return $this;
    }

    private function stripDots(string $name): string
    {
        return str_replace('.', '', $name);
    }

    private function sanitiseTitle(string $title): string
    {
        $sanitised = $title;

        switch ($title) {
            case 'Mister':
                $sanitised = 'Mr';
                break;
        }

        return $sanitised;
    }
}
