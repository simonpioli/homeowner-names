@extends('layouts.app')
@section('content')
    <div id="app">

        <noscript><p class="strong">Please enable Javascript to use this website.</p></noscript>

        <router-view></router-view>
    </div>
@endsection
