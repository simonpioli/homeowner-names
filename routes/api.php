<?php

use App\Http\Controllers\Api\HomeownerNamesController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/names', [HomeownerNamesController::class, 'index'])->name('homeowners.index');
Route::post('/names/store', [HomeownerNamesController::class, 'store'])->name('homeowners.store');
