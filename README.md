## Setting up
* Uses Laravel 7.* so requires PHP >= 7.2.5 and usual Laravel extensions plus JSON
* Requires Redis, used as temporary storage instead of firing up a whole database thing!
* All settings required to run the app are in the `.env.local` file.
* I've used Homestead to run it with my own URL settings. YMMV.

## Notes
* I've made assumptions around the formatting of married couples. The supplied data conforms to convention but other data might not.
* I've added 2 extra keys to the person, one to check for a married partnership, and the other to highlight potential issues if the supplied data isn't exactly the same as the supplied examples.

## Improvements
* An inline form could be provided to manually adjust entries that are incorrect.
* Do we need to have a debate on where I've caught the exceptions? I've done it in the controller as that's where the final response to the user would be decided.
